FROM postgres:12

ENV TZ=Europe/Moscow \
  DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
  apt-get install -y build-essential git postgresql-server-dev-12 python3 && \
  git clone -b master --depth=1 https://github.com/petere/pguint.git && \
  cd /pguint && make && make install && rm -rf /pguint && \
  apt-get purge -y build-essential git postgresql-server-dev-12 python3 && \
  apt-get autoremove -y && apt-get clean all -y
